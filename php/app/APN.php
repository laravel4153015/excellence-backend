<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class APN extends Model
{
    protected $table = 'a_p_n_s';
    protected $fillable = ['apn_number','ip','status','inactive_reason','carrier','date_deployed','comment'];

    
    public function saccos() {
        return $this->belongsToMany(Sacco::class, 'a_p_n_sacco', 'apn_id', 'sacco_id')->withTimestamps();
    }
    
}
