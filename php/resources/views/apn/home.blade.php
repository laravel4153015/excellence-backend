<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style>
	.container {
		margin-top: 50px;
	}
	h1 {
		text-align: center;
		margin-bottom: 30px;
	}
	.btn-secondary {
		margin-bottom: 10px;
	}
	</style>
</head>
<body>
	<div class="container">
		<h1>Home</h1>
	<!-- Button to upload data -->
	<div class="form-group">
		<a href="{{ route('upload') }}" class="btn btn-secondary">Upload/Update Excel Sheet</a>
	</div>

	<!-- Button to verify data -->
	<div class="form-group">
		<a href="{{ route('verify') }}" class="btn btn-secondary">Verify APNs</a>
	</div>

	<!-- Button to edit data with excel interface -->
	<div class="form-group">
		<a href="{{ route('edit') }}" class="btn btn-secondary">Edit Excel sheet with interface</a>
	</div>

	<!-- Button to search for SACCOs and APNS -->
	<div class="form-group">
		<a href="{{ route('search') }}" class="btn btn-secondary">Search for APNs/SACCOs</a>
	</div>


	<!-- Button to download data -->
	<div class="form-group">
		<a href="{{ route('download') }}" class="btn btn-secondary">Download Latest Excel sheet</a>
	</div>
	
</div>
<!-- Add a div to display the Excel sheet -->
<div id="excel-sheet-container"></div>

<!-- Include the JavaScript code -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html> 