<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Collection;
use App\APN;
use App\Sacco;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class APNImport implements ToModel, SkipsOnError, WithHeadingRow
{
    use SkipsErrors;

    protected $data = [];
    protected $failures = [];
    protected $isVerification;

    public function __construct(bool $isVerification = false)
    {
        $this->isVerification = $isVerification;
    }

    public function collection(Collection $rows)
    {
        $columnMapping = [
            'service_info' => 'apn_number',
            'service_id' => 'apn_number',
        ];

        foreach ($rows as $row) {
            if (!$row->has('service_info') && !$row->has('service_id')) {
                continue;
            }

            $rowData = $row->toArray();

            $newRowData = [];
            foreach ($rowData as $key => $value) {
                $columnName = isset($columnMapping[$key]) ? $columnMapping[$key] : $key;
                $newRowData[$columnName] = $value;
            }

            if ($this->isVerification) {
                $apnValue = $newRowData['apn_number'] ?? null;

                if (empty($apnValue) || preg_match('/^[A-Za-z]/', $apnValue)) {
                    continue;
                }

                $apn = APN::where('apn_number', $apnValue)->first();

                if (!$apn) {
                    Log::info('Processing: ' . $apnValue);
                    $this->failures[] = 'APN does not exist: ' . $apnValue;
                } elseif ($apn->status !== 'Active') {
                    Log::info('Processing: ' . $apnValue);
                    $this->failures[] = 'APN exists but is inactive: ' . $apnValue;
                }
            } else {
                // Data Import mode
                $validator = Validator::make($newRowData, [
                    'apn_number' => 'required|digits:9|unique:a_p_n_s',
                    'status' => 'nullable|in:Active,Inactive,Deleted,inactive,active',
                    'inactive_reason' => 'nullable|string|max:255',
                    'carrier' => 'nullable|string|max:255',
                    'ip' => 'nullable|string|ip|unique:a_p_n_s',
                    'sacco_name' => 'nullable|string',
                    'person_in_charge' => 'nullable|digits:9',
                ], [
                    'apn_number.required' => 'The APN number is required.',
                    'apn_number.digits' => 'The APN number must be 9 digits.',
                    'apn_number.unique' => 'The APN number is already taken.',
                    'status.in' => 'Invalid status value.',
                    'inactive_reason.max' => 'The inactive reason must not exceed 255 characters.',
                    'carrier.max' => 'The carrier must not exceed 255 characters.',
                    'ip.ip' => 'Invalid IP address format.',
                    'ip.unique' => 'The IP address is already taken.',
                    'person_in_charge.digits' => 'The Person In Charge number must be 9 digits.',
                ]);

                if ($validator->fails()) {
                    $this->failures[] = $validator->errors()->all();
                    continue;
                }

                $this->data[] = $row;
            }
        }
    }

    public function model(array $row)
    {
        if ($this->isVerification) {
            return null;
        }

        $apnValue = $row['service_info'] ?? $row['service_id'] ?? null;
        $saccoName = isset($lookupTable[$apnValue]) ? $lookupTable[$apnValue] : null;

        return new APN([
            'apn_number' => $apnValue,
            'status' => $row['status'] ?? '',
            'inactive_reason' => $row['inactive_reason'] ?? '',
            'carrier' => $row['carrier'] ?? '',
            'ip' => $row['ip'] ?? '',
            'sacco_name' => $row['sacco_name'] ?? $row['sacco_using_the_apn'] ?? '',
            'person_in_charge' => $row['person_in_charge'] ?? '',
        ]);
    }

    public function getData()
    {
        return $this->data;
    }

    public function getFailures()
    {
        return $this->failures;
    }
}
