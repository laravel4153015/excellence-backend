#!/bin/sh

# Set permissions
chmod -R 777 /var/run/php
chmod -R 777 /var/www/backend-assessment/excel-app-v2
chmod -R 777 /var/www/backend-assessment/api-frontend

# Start services
service php-fpm start
service nginx start

# Start Python script in the background
python3 /var/www/backend-assessment/excelapp.py &

# Navigate to the frontend directory and start npm
cd /var/www/backend-assessment/api-frontend
npm run serve
