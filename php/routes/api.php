<?php
// header("Access-Control-Allow-Origin", "*");
// header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
// header("Access-Control-Allow-Headers", "Content-Type");

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\APN;
use App\Sacco;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('/', 'APNController@index')->name('index');
Route::post('/upload', 'APNController@upload')->name('upload');
Route::post('/verify','APNController@verify')->name('verify');
Route::get('/download', 'APNController@downloadFile')->name('download');
// Route::post('/edit', 'APNController@editData')->name('edit');
Route::post('/edit', 'APNController@editData')->name('edit');
Route::post('/delete', 'APNController@deleteData')->name('delete');
Route::post('/search', 'APNController@search')->name('search');
Route::post('/modify', 'APNController@uploadAndModifyExcel')->name('uploadAndModifyExcel');
Route::delete('/delete/{apn_id}','APNController@deleteAPN')->name('delete');
