// Load the Excel file using SheetJS
var file = XLSX.readFile('data.xlsx');

// Convert the Excel data to a JSON object
var data = XLSX.utils.sheet_to_json(file.Sheets['Sheet1']);

// Display the data in a table on the page
var table = document.createElement('table');
var header = table.createTHead();
var row = header.insertRow(0);
for (var key in data[0]) {
	var th = document.createElement('th');
	th.innerHTML = key;
	row.appendChild(th);
}
var tbody = table.createTBody();
for (var i = 0; i < data.length; i++) {
	var row = tbody.insertRow(i);
	for (var key in data[i]) {
		var cell = row.insertCell(-1);
		cell.innerHTML = data[i][key];
	}
}
document.getElementById('table-container').appendChild(table);

// Add a click handler to the Save button
document.getElementById('save-button').addEventListener('click', function() {
	// Convert the edited data back to an Excel file using SheetJS
	var editedData = [];
	var rows = document.getElementsByTagName('tr');
	for (var i = 1; i < rows.length; i++) {
		var rowData = {};
		var cells = rows[i].getElementsByTagName('td');
		for (var j = 0; j < cells.length; j++) {
			var headerText = rows[0].getElementsByTagName('th')[j].innerHTML;
			rowData[headerText] = cells[j].innerHTML;
		}
		editedData.push(rowData);
	}
	var editedFile = XLSX.utils.book_new();
	var editedSheet = XLSX.utils.json_to_sheet(editedData);
	XLSX.utils.book_append_sheet(editedFile, editedSheet, 'Sheet1');
	XLSX.writeFile(editedFile, 'edited-data.xlsx');
