<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UpdateAPNRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Illuminate\Validation\Rule;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception as SpreadsheetReaderException;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\Zip;
use Symfony\Component\HttpFoundation\StreamedResponse;
use PHPExcel_IOFactory;
use App\APN;
use App\Imports\APNImport;
use App\Sacco;
use Carbon\Carbon;

use function mb_convert_encoding;


error_reporting(E_ALL & ~E_DEPRECATED);

class APNController extends Controller
{   
    public function index()
    {
        $apns = APN::with('saccos')->orderBy('status')->get();
        return response()->json(['data' => $apns]);
    }   

    
    public function verify(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:xlsx,xls',
            'file.*.apn_number' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()
            ], 422);
        }

        $file = $request->file('file')->getRealPath();
        $data = Excel::load($file, function ($reader) {
        })->get();
        $errors = [];
        
        foreach ($data as $row) {
            // $rowData = array_intersect_key($row->toArray(), array_flip(['service_info', 'service_id']));

            // if (empty($rowData)) {
            //     continue; // Skip rows without 'service_info' or 'service_id'
            // }
            // dd($row);
            $apnValue = $row->service_id ?? $row->service_info;
        
        
            if (empty($apnValue) || preg_match('/^[A-Za-z]/', $apnValue)) {
                continue; 
            }
        
            $apn = APN::where('apn_number', $apnValue)->first();
            if (!$apn) {
                $errors[] = 'APN does not exist: ' . $apnValue;
            } elseif ($apn->status !== 'Active') {
                $errors[] = 'APN exists but is inactive: ' . $apnValue;
            }
        }

        if (count($errors) === 0) {
            return response()->json([
                'success' => true,
                'message' => 'All APNs exist in the database'
            ]);
        }
        
        return response()->json([
            'success' => false,
            'errors' => $errors,
        ], 200);
    }

    public function upload(Request $request)
    {
        error_reporting(E_ALL & ~E_DEPRECATED);

        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:xlsx,xls,csv'
        ],[
            'file.required' => 'Please select a file.',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()
            ], 422);
        }

        $file = $request->file('file')->getRealPath();
        $data = Excel::load($file, function ($reader) {
        })->get();
        $errors = [];
        $columnMapping = [
            'service_info' => 'apn_number',
            'service_id' => 'apn_number',
            'sacco_using_the_apn' => 'sacco_name'
        ];
        
        foreach ($data as $row) {
            if (!$row->has('service_info') && !$row->has('service_id')) {
                continue;
            }                      
        
            $rowData = $row->toArray();
            $apn_number = $row->apn_number;
            if (preg_match('/[A-Za-z]/', $apn_number)) {
                continue; // Skip rows with alphabetic characters in apn_number
            }
        
            $newRowData = [];
            foreach ($rowData as $key => $value) {
                $columnName = isset($columnMapping[$key]) ? $columnMapping[$key] : $key;
                $newRowData[$columnName] = $value;
            }
            $validator = Validator::make($newRowData, [
                'apn_number' => 'required|digits:9|unique:a_p_n_s',
                'status' => 'nullable|in:Active,Inactive,Deleted,inactive,active',
                'inactive_reason' => 'nullable|string|max:255',
                'carrier' => 'nullable|string|max:255',
                'ip' => 'nullable|string',
                'sacco_name' => 'nullable|string',
                'person_in_charge' => 'nullable|max:255',
            ], [
                'apn_number.required' => 'The APN number is required.',
                'apn_number.digits' => 'The APN number must be 9 digits.',
                'apn_number.unique' => 'The APN number is already taken.',
                'status.in' => 'Invalid status value.',
                'inactive_reason.max' => 'The inactive reason must not exceed 255 characters.',
                'carrier.max' => 'The carrier must not exceed 255 characters.',
            ]);
            
    
            if ($validator->fails()) {
                $errors[] = $validator->errors()->all();
                continue;
            }

            $apn = APN::where('apn_number', $row->apn_number)->first();

            if (!$apn) {
                $apn = new APN;
                $apn->apn_number = ucwords(trim(htmlspecialchars($row->service_info ?? $row->service_id)));
                $apn->status =  'Active' ?? ucwords(trim(htmlspecialchars($row->status)));
                $apn->inactive_reason = ucwords(trim(htmlspecialchars($row->inactive_reason)));
                $apn->carrier = ucwords(trim(htmlspecialchars($row->carrier)));
                $apn->ip = $row->ip;
                $apn->date_deployed = ucwords(trim(htmlspecialchars($row->date_deployed)));
                $apn->comment = ucwords(trim(htmlspecialchars($row->comment)));
                $apn->save();

                // Create or update the associated Sacco
                $sacco = Sacco::where('sacco_name', $row->sacco_name ?? $row->sacco_using_the_apn)->first();

                if (!$sacco) {
                    $sacco = new Sacco;
                    $sacco->sacco_name = ucwords(trim(htmlspecialchars($row->sacco_name ?? $row->sacco_using_the_apn)));
                    $sacco->person_in_charge = ucwords(trim(htmlspecialchars($row->person_in_charge)));
                    $sacco->save();
                }else {
                    // If the Sacco already exists, update its fields
                    $sacco->update([
                        'sacco_name' => ucwords(trim(htmlspecialchars($row->sacco_name ?? $row->sacco_using_the_apn))),
                        'person_in_charge' => ucwords(trim(htmlspecialchars($row->person_in_charge))),
                    ]);
                }

                // Attach the APN to the Sacco
                $sacco->apns()->syncWithoutDetaching([$apn->id]);
            } else {
                $errors[] = 'APN already exists: ' . ($row->service_info ?? $row->service_id);
            }
        }

        if (count($errors) === 0) {
            return response()->json([
                'success' => true,
                'message' => 'APNs uploaded successfully'
            ]);
        }

        return response()->json([
            'success' => false,
            'errors' => $errors
        ], 409);
    }

    public function downloadFile(Request $request)
    {
        $data = APN::with('saccos')->get();
        $dataArray = [];

        foreach ($data as $apn) {
            foreach ($apn->saccos as $sacco) {
                $dataArray[] = [
                    'Sacco Name' => $sacco->sacco_name ,
                    'Person In Charge' => $sacco->person_in_charge,
                    'APN Number' => $apn->apn_number,
                    'Status' => $apn->status,
                    'Inactive Reason' => $apn->inactive_reason,
                    'Carrier' => $apn->carrier,
                    'IP' => $apn->ip,
                ];
            }
        }

        $filename = 'modified_data_' . time();
        $excelFile = Excel::create($filename, function ($excel) use ($dataArray) {
            $excel->sheet('sheet1', function ($sheet) use ($dataArray) {
                $sheet->fromArray($dataArray);
            });
        })->store('xlsx', false, true);

        return response()->download(storage_path('exports/' . $excelFile['file']));
    }
    
    public function editData(Request $request)
    {
        $data = $request->all();
        $filteredData = array_filter($data, function ($row) {
            // Filter out rows where all cells are empty
            return !empty(array_filter($row));
        });
        $deletedRows = [];
        foreach ($filteredData as $row) {
            $id = htmlspecialchars($row['id']);
            $sacco_id = htmlspecialchars($row['sacco_id']);
            $sacco_name = trim(htmlspecialchars($row['sacco_name']));
            $person_in_charge = trim(htmlspecialchars($row['person_in_charge']));
            $apn_number = trim(htmlspecialchars($row['apn_number']));
            $status = isset($row['status']) ? trim(htmlspecialchars($row['status'])) : '';
            $inactive_reason = trim(htmlspecialchars($row['inactive_reason']));
            $carrier = trim(htmlspecialchars($row['carrier']));
            $ip = trim(htmlspecialchars($row['ip']));
            $date_deployed = trim(htmlspecialchars($row['date_deployed'])); 
            $comment = trim(htmlspecialchars($row['comment']));

            if (isset($row['htRemoveRow'])) {
                // Row marked for deletion
                $deletedRows[] = $id;
                continue;
            }

            $collection = [
                'sacco_name' => htmlspecialchars(ucwords($sacco_name)),
                'person_in_charge' => ucwords($person_in_charge),
                'apn_number' => ucwords($apn_number),
                'status' => ucwords($status),
                'inactive_reason' => ucwords($inactive_reason),
                'carrier' => strtoupper($carrier),
                'ip' => $ip,
            ];

            $validator = Validator::make($collection, [
                'sacco_name' => 'nullable|string',
                'person_in_charge' => 'nullable|string|max:255',
                'apn_number' => 'required|string|max:11',
                'status' => 'nullable|in:Active,Inactive,Deleted',
                'inactive_reason' => 'nullable|string|max:255',
                'carrier' => 'nullable|string|max:100',
                'ip' => 'nullable|string', #|ip',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'errors' => $validator->errors(),
                    'row' => $id
                ], 500);
            }

            $apn = APN::updateOrCreate(['id' => $id], [
                'apn_number' => $apn_number,
                'ip' => $ip,
                'status' => $status,
                'inactive_reason' => $inactive_reason,
                'carrier' => $carrier,
                'date_deployed'=> $date_deployed,
                'comment' => $comment,
            ]);

            $sacco = Sacco::updateOrCreate(['id' => $sacco_id], [
                'sacco_name' => $sacco_name,
                'person_in_charge' => $person_in_charge,
            ]);

            if ($apn && $sacco) {
                $sacco->apns()->syncWithoutDetaching([$apn->id]);
            }
        }

        // Perform deletion of rows
        if (!empty($deletedRows)) {
            APN::whereIn('id', $deletedRows)->delete();
        }

        return response()->json([
            'success' => true,
            'message' => 'File saved successfully!',
            'data'=> $data
        ]);
    }


    public function search(Request $request)
    {
        $search = $request->input('search');
        $search = ltrim($search, '0');
        $request->validate([
            'search' => 'required'
        ]);

        // Search by Sacco name
        $saccoResults = Sacco::where('sacco_name', 'like', '%' . $search . '%')
            ->withCount('apns')
            ->with(['apns' => function ($query) {
                $query->select('a_p_n_s.id', 'a_p_n_s.apn_number', 'a_p_n_s.ip','a_p_n_s.status','a_p_n_s.inactive_reason','a_p_n_s.carrier','a_p_n_s.date_deployed','a_p_n_s.comment');
            }])
            ->get(['saccos.id', 'saccos.sacco_name', 'saccos.person_in_charge']);

        // Search by apn_number
        $apnNumberResults = Sacco::whereHas('apns', function ($query) use ($search) {
            $query->where('a_p_n_s.apn_number', 'like', '%' . $search . '%');
        })
            ->with(['apns' => function ($query) {
                $query->select('a_p_n_s.id', 'a_p_n_s.ip','a_p_n_s.apn_number', 'a_p_n_sacco.sacco_id','a_p_n_s.status','a_p_n_s.carrier','a_p_n_s.inactive_reason','a_p_n_s.date_deployed','a_p_n_s.comment');
            }])
            ->get(['saccos.id', 'saccos.sacco_name', 'saccos.person_in_charge']);

        // Search by IP
        $ipResults = Sacco::whereHas('apns', function ($query) use ($search) {
            $query->where('a_p_n_s.ip', '=',$search);
        })
            ->with(['apns' => function ($query) {
                $query->select('a_p_n_s.id', 'a_p_n_s.apn_number', 'a_p_n_sacco.sacco_id','a_p_n_s.status','a_p_n_s.ip','a_p_n_s.inactive_reason','a_p_n_s.carrier');
            }])
            ->get(['saccos.sacco_name']);

        // Display the search results
        if ($saccoResults->isEmpty() && $apnNumberResults->isEmpty() && $ipResults->isEmpty()) {
            $errorMessage = 'No results found';
            return response()->json([
                'success' => false,
                'message' => $errorMessage
            ]);
        }

        return response()->json([
            'success' => true,
            'saccoResults' => $saccoResults,
            'apnNumberResults' => $apnNumberResults,
            'ipResults' => $ipResults
        ]);
    }

    public function deleteData(Request $request)
    {
        $deletedRows = $request->input('deletedRows');

        if (empty($deletedRows)) {
            return response()->json([
                'success' => false,
                'message' => 'No rows selected for deletion.',
            ], 400);
        }

        APN::whereIn('id', $deletedRows)->update(['status' => 'Deleted']);

        return response()->json([
            'success' => true,
            'message' => 'Rows deleted successfully!',
        ]);
    }

}




