<!DOCTYPE html>
<html>
<head>
    <title>Update APN Data</title>
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Update Data') }}</div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif

                        @if ($errors && count($errors) > 0)
                            <div class="alert alert-danger" role="alert">
                                <ul>
                                    @foreach ($errors as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form method="POST" action="{{ route('update') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <label for="file">{{ __('Upload Excel File') }}</label>
                                <input id="file" type="file" class="form-control @error('file') is-invalid @enderror" name="file" required>
                                {{-- @error('file')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                                @enderror --}}
                            </div>

                            <button type="submit" class="btn btn-primary">{{ __('Update Data') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>