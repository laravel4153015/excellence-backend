from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS,cross_origin
from openpyxl import load_workbook
import tempfile
from werkzeug.utils import secure_filename
from sqlalchemy.exc import IntegrityError
from flask_marshmallow import Marshmallow
import pymysql
pymysql.install_as_MySQLdb()
from marshmallow import Schema, fields
import pandas as pd
from datetime import datetime
from sqlalchemy.sql import func
from dotenv import load_dotenv
import os
import logging

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)


dotenv_path = './.env.py'
load_dotenv(dotenv_path)

db_user = os.getenv('DB_USERNAME')
db_password = os.getenv('DB_PASSWORD')
db_host = os.getenv('DB_HOST','localhost')
db_name = os.getenv('DB_DATABASE')

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql://{db_user}:{db_password}@{db_host}/{db_name}'
db = SQLAlchemy(app)
ma = Marshmallow(app)
CORS(app)


class APN(db.Model):
    __tablename__ = 'a_p_n_s'
    id = db.Column(db.Integer, primary_key=True)
    apn_number = db.Column(db.String(255), unique=False, nullable=False)
    status = db.Column(db.String(10),default='Active')
    inactive_reason = db.Column(db.String(255))
    carrier = db.Column(db.String(255),default='MTN')
    ip = db.Column(db.String(100))  #,unique=True)
    date_deployed = db.Column(db.String(100))
    comment = db.Column(db.String(255))

    created_at = db.Column(db.DateTime, default=func.convert_tz(func.now(), 'UTC', 'Africa/Nairobi'))
    updated_at = db.Column(db.DateTime, default=func.convert_tz(func.now(), 'UTC', 'Africa/Nairobi'), onupdate=func.convert_tz(func.now(), 'UTC', 'Africa/Nairobi'))

    saccos = db.relationship('Sacco', secondary='a_p_n_sacco', back_populates='apns')

class Sacco(db.Model):
    __tablename__ = 'saccos'
    id = db.Column(db.Integer, primary_key=True)
    sacco_name = db.Column(db.String(255), unique=False)
    person_in_charge = db.Column(db.String(255))

    created_at = db.Column(db.DateTime, default=func.convert_tz(func.now(), 'UTC', 'Africa/Nairobi'))
    updated_at = db.Column(db.DateTime, default=func.convert_tz(func.now(), 'UTC', 'Africa/Nairobi'), onupdate=func.convert_tz(func.now(), 'UTC', 'Africa/Nairobi'))

    apns = db.relationship('APN', secondary='a_p_n_sacco', back_populates='saccos')


class APNSacco(db.Model):
    __tablename__ = 'a_p_n_sacco'
    apn_id = db.Column(db.Integer, db.ForeignKey('a_p_n_s.id'), primary_key=True)
    sacco_id = db.Column(db.Integer, db.ForeignKey('saccos.id'), primary_key=True)

    created_at = db.Column(db.DateTime, default=func.convert_tz(func.now(), 'UTC', 'Africa/Nairobi'))
    updated_at = db.Column(db.DateTime, default=func.convert_tz(func.now(), 'UTC', 'Africa/Nairobi'), onupdate=func.convert_tz(func.now(), 'UTC', 'Africa/Nairobi'))

    apn = db.relationship('APN', backref=db.backref('a_p_n_sacco', cascade='all, delete-orphan'))
    sacco = db.relationship('Sacco', backref=db.backref('a_p_n_sacco', cascade='all, delete-orphan'), overlaps="apns,saccos")
    

class APNSchema(ma.Schema):
    class Meta:
        fields = ('id', 'apn_number', 'status', 'inactive_reason', 'carrier', 'ip','date_deployed', 'comment')

class SaccoSchema(ma.Schema):
    class Meta:
        fields = ('id', 'sacco_name', 'person_in_charge')

class APNSaccoSchema(ma.Schema):
    class Meta:
        fields = ('apn_id', 'sacco_id')

apn_schema = APNSchema()
sacco_schema = SaccoSchema()
apn_sacco_schema = APNSaccoSchema()

with app.app_context():
    db.create_all()

@app.route('/api/', methods=['GET'])
def get_apns():
    try:
        apns = APN.query.join(APNSacco).join(Sacco).all()

        results = []
        for apn in apns:
            apn_data = {
                'id': apn.id,
                'apn_number': apn.apn_number,
                'status' : apn.status,
                'inactive_reason' : apn.inactive_reason, 
                'carrier' : apn.carrier, 
                'ip': apn.ip,
                'date_deployed': apn.date_deployed,
                'comment': apn.comment,
                'saccos': [{'sacco_id': sacco.id, 'sacco_name': sacco.sacco_name,'person_in_charge':sacco.person_in_charge} for sacco in apn.saccos]
            }
            results.append(apn_data)

        return jsonify({"data":results}), 200

    except Exception as e:
        return jsonify({'error': str(e)}), 500

@app.route('/api/verify', methods=['POST'])
def verify_apns():
    file = request.files.get('file', None)
    if not file:
        return jsonify({'success': False, 'errors': {'file': ['Please select a file.']}}), 422

    if not file.filename.lower().endswith(('.xlsx', '.xls', '.csv')):
        return jsonify({'success': False, 'errors': {'file': ['Invalid file format. Only xlsx, xls, and csv files are allowed.']}}), 422

    try:
        columns_to_read = "A:K"

        df = pd.read_excel(file, engine='openpyxl', usecols=columns_to_read) #only for docker centos7 container, can exclude for pandas >=1.2
                
        df.columns = df.columns.str.strip().str.lower()

        if 'apn number' in df.columns:
            service_column = 'apn number'         
        elif 'service id' not in df.columns:
            service_column = 'service_info'
        else:
            service_column = 'service id'

        df = df.dropna()

        service_ids = df[service_column].tolist()

        errors = []
        for service_id in service_ids:

            apn = APN.query.filter_by(apn_number=service_id).first()
            if not apn:
                errors.append('APN does not exist for service ID: ' + str(service_id))
            elif apn.status != 'Active':
                errors.append('APN exists but is inactive for service ID: ' + str(service_id))

        if not errors:
            return jsonify({'success': True, 'message': 'All service IDs exist in the database'})

        return jsonify({'success': False, 'errors': errors}), 200

    except Exception as e:
        return jsonify({'success': False, 'errors': {'file': [str(e)]}}), 422


@app.route('/api/upload', methods=['POST'])
def upload_apns():
    file = request.files.get('file', None)
    if not file:
        return jsonify({'success': False, 'errors': {'file': ['Please select a file.']}}), 422

    if not file.filename.lower().endswith(('.xlsx', '.xls', '.csv')):
        return jsonify({'success': False, 'errors': {'file': ['Invalid file format. Only xlsx, xls, and csv files are allowed.']}}), 422

    try:
        import os
        temp_dir = tempfile.mkdtemp()
        filename = secure_filename(file.filename)
        file_path = os.path.join(temp_dir, filename)
        file.save(file_path)

        workbook = load_workbook(file_path, read_only=True,data_only=True)
        sheet = workbook.worksheets[0]

        errors = []
        column_mapping = {
            'SERVICE ID': 'apn_number',
            'SACCO NAME':'sacco_name',
            'PERSON IN CHARGE':'person_in_charge',
            'DATE DEPLOYED':'date_deployed',
            'COMMENT':'comment',
            'IP':'ip',
            'CARRIER':'carrier'
        }

        sacco_cache = {}

        for row in sheet.iter_rows(values_only=True, min_row=2,max_col=8):
            if not any(row):
                continue
            new_row_data = {}

            for column_name in column_mapping.values():
                new_row_data[column_name] = None  

            for index, cell_value in enumerate(row, start=1):
                column_name = sheet.cell(row=1, column=index).value
                column_name = column_mapping.get(column_name, None)
                
                if column_name and cell_value is not None:  
                    new_row_data[column_name] = cell_value

            logger.info("Processing row: %s", row)


            if not new_row_data.get('apn_number', None):
                logger.info("New row data: %s", new_row_data)
                continue  

            sacco_name = new_row_data.pop('sacco_name', None)
            person_in_charge = new_row_data.pop('person_in_charge', None)
            if sacco_name:
                sacco = Sacco.query.filter_by(sacco_name=sacco_name).first()
                if not sacco:
                    sacco = Sacco(sacco_name=sacco_name, person_in_charge=person_in_charge)
                    db.session.add(sacco)
                    db.session.flush()
                sacco_cache[sacco_name] = sacco
            
            # validator = apn_schema.validate(new_row_data)
            # if validator:
            #     errors.append(validator)
            #     continue

            apn_number = new_row_data.get('apn_number', None)
            status = new_row_data.get('status', None)
            carrier  = new_row_data.get('carrier', None)
            inactive_reason = new_row_data.get('inactive_reason', None)
            ip = new_row_data.get('ip', None)
            date_deployed = new_row_data.get('date_deployed', None)
            comment = new_row_data.get('comment', None)
            
            apn = APN.query.filter_by(apn_number=apn_number).first()

            if not apn:
                apn = APN(apn_number=apn_number,status=status, carrier=carrier, inactive_reason=inactive_reason, ip=ip, date_deployed=date_deployed, comment=comment)
                db.session.add(apn)
                db.session.flush()

            if sacco_name:
                apn_sacco = APNSacco(apn_id=apn.id, sacco_id=sacco.id)
                db.session.add(apn_sacco)
            logger.info("Row processed successfully")

    except IntegrityError as e:
        db.session.rollback()  
        errors.append(str(e))
        logger.error("IntegrityError: %s", e)
    except Exception as e:
        db.session.rollback()  
        logger.error("Exception: %s", e)
        return jsonify({'success': False, 'errors': {'file': [str(e)]}}), 422
    
    db.session.commit()

    if not errors:
        return jsonify({'success': True, 'message': 'APNs uploaded successfully'})

    return jsonify({'success': False, 'errors': errors}), 409


@app.route('/api/edit', methods=['POST'])
def edit_data():
    data = request.get_json()
    filtered_data = [row for row in data if any(row.values())]

    deleted_rows = []
    for row in filtered_data:
        row_id = row.get('id')
        sacco_id = row.get('sacco_id')
        sacco_name = row.get('sacco_name', '').strip()
        person_in_charge = row.get('person_in_charge', '')
        if isinstance(person_in_charge, str):
            person_in_charge = person_in_charge.strip()
        else:
            person_in_charge = ''
        apn_number = row.get('apn_number', '').strip()
        status = row.get('status', '').strip()
        inactive_reason = row.get('inactive_reason', '')
        if isinstance(inactive_reason, str):
            inactive_reason = inactive_reason.strip()
        else:
            inactive_reason = ''
        carrier = row.get('carrier', '').strip()
        ip = row.get('ip', '')
        if isinstance(ip, str):
            ip = ip.strip()
        else:
            ip = ''
        date_deployed = row.get('date_deployed', '')
        if isinstance(date_deployed, str):
            date_deployed = date_deployed.strip()
        else:
            date_deployed = ''
        comment = row.get('comment', '')
        if isinstance(comment, str):
            comment = comment.strip()
        else:
            comment = ''

        if row.get('htRemoveRow'):
            deleted_rows.append(row_id)
            continue

        collection = {
            'sacco_name': sacco_name.title(),
            'person_in_charge': person_in_charge.title(),
            'apn_number': apn_number.title(),
            'status': status.title(),
            'inactive_reason': inactive_reason.title(),
            'carrier': carrier.upper(),
            'ip': ip,
            'date_deployed': date_deployed,
            'comment': comment
        }

        schema = APNSaccoSchema()
        errors = schema.validate(collection)
        if errors:
            return jsonify({
                'success': False,
                'errors': errors,
                'row': row_id
            }), 500

        apn = APN.query.filter_by(id=row_id).first()
        if apn:
            apn.apn_number = apn_number
            apn.ip = ip
            apn.status = status
            apn.inactive_reason = inactive_reason
            apn.carrier = carrier
            apn.date_deployed = date_deployed
            apn.comment = comment
        else:
            apn = APN(
                id=row_id,
                apn_number=apn_number,
                ip=ip,
                status=status,
                inactive_reason=inactive_reason,
                carrier=carrier,
                date_deployed=date_deployed,
                comment=comment
            )
            db.session.add(apn)

        sacco = Sacco.query.filter_by(id=sacco_id).first()
        if sacco:
            sacco.sacco_name = sacco_name
            sacco.person_in_charge = person_in_charge
        else:
            sacco = Sacco(
                id=sacco_id,
                sacco_name=sacco_name,
                person_in_charge=person_in_charge
            )
            db.session.add(sacco)

        if apn and sacco:
            sacco.apns.append(apn)

    if deleted_rows:
        APN.query.filter(APN.id.in_(deleted_rows)).delete()

    db.session.commit()

    return jsonify({
        'success': True,
        'message': 'File saved successfully!',
        'data': data
    }), 200

@app.route('/api/search', methods=['POST'])
def search():
    search = request.form.get('search')
    if not search:
        return jsonify({
            'success': False,
            'message': 'No search query provided'
        })

    saccoResults = db.session.query(Sacco.id, Sacco.sacco_name, Sacco.person_in_charge, APN.id, APN.apn_number, APN.ip) \
        .select_from(Sacco) \
        .filter(Sacco.sacco_name.ilike('%' + search + '%')) \
        .outerjoin(APNSacco, APNSacco.sacco_id == Sacco.id) \
        .outerjoin(APN, APNSacco.apn_id == APN.id) \
        .all()

    apn_numberResults = db.session.query(Sacco.id, Sacco.sacco_name, Sacco.person_in_charge, APN.id, APN.ip, APNSacco.sacco_id) \
        .select_from(Sacco) \
        .filter(APN.apn_number.ilike('%' + search + '%')) \
        .join(APNSacco, APNSacco.sacco_id == Sacco.id) \
        .join(APN, APNSacco.apn_id == APN.id) \
        .all()

    ipResults = db.session.query(Sacco.sacco_name, APN.id, APN.apn_number, APNSacco.sacco_id) \
        .select_from(Sacco) \
        .filter(APN.ip.ilike('%' + search + '%')) \
        .join(APNSacco, APNSacco.sacco_id == Sacco.id) \
        .join(APN, APNSacco.apn_id == APN.id) \
        .all()

    saccoResults = [dict(row) for row in saccoResults]
    apn_numberResults = [dict(row) for row in apn_numberResults]
    ipResults = [dict(row) for row in ipResults]

    if not saccoResults and not apn_numberResults and not ipResults:
        errorMessage = 'No results found'
        return jsonify({
            'success': False,
            'message': errorMessage
        })

    return jsonify({
        'success': True,
        'saccoResults': saccoResults,
        'apn_numberResults': apn_numberResults,
        'ipResults': ipResults
    })


if __name__ == '__main__':
    with app.app_context():
        db.create_all()
    app.run(debug=True,host='0.0.0.0',port='8001')
