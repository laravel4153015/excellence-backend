

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Download Excel Sheet</title>
</head>
<body>
    <div class="container">
        <h1>Download Modified Excel Sheer</h1>
        <p>Click the button below to download the modified data as an Excel file:</p>
        <a href="{{ route('download.file') }}" class="btn btn-primary">Download Data</a>
    </div>
</body>
</html>