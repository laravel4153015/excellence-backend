# WELCOME TO THE EXCELLENCE API V1.0!
## Functionality
The Excellence App as its name states is all about uploading excel sheets, parse the data and storing that data in a database.
This data is split into 3 tables that mamage the data i.e one for APNs, one for Saccos and one pivot table linking the two.
This data can be easily accessed through simple queries and edited through various features(given you're authorized).
## Setup

### Clone repository
### Install all composer dependencies
```
Run composer install
```

### Setup the database and run the migrations
```
php artisan migrate
```
### Start the php server locally
```
php artisan serve
```
### Use your favoured API request platform e.g. Postman,ThunderClient

## APNController API Documentation

This documentation provides information about the APIs available in the APNController.
Index APNs

## Get a list of all APNs.

Endpoint: /api/

Method: GET

Response Format: JSON

Parameters: None

Response

    * Success (200 OK)
        + Returns a JSON object with the following data:
            + data (array): Array of APN objects containing the following fields:
              +  id (integer): The APN ID.
              +  apn_number (string): The APN number.
              +  status (string): The APN status.
              +  inactive_reason (string): The reason for APN inactivity.
              +  carrier (string): The APN carrier.
              +  ip (string): The APN IP.
              +  saccos (array): Array of associated Sacco objects.

    * Error (Other status codes)
        + Returns an error response with the appropriate status code.

## Verify APNs

Verify the existence of APNs using a file upload.

Endpoint: /api/verify

Method: POST

Request Format: Multipart form-data

Parameters: 

    + file (file): The Excel file containing APN numbers.
    (Form-url-encoded)
Response

    * Success (200 OK)
        + Returns a JSON object with the following data:
            + success (boolean): true if all APNs exist, false otherwise.
            + message (string): The result message.

    * Error (Other status codes)
        + Returns an error response with the appropriate status code and error details.

## Upload APNs

Upload APNs using a file.

Endpoint: /api/upload

Method: POST

Request Format: Multipart form-data

Parameters:

    file (file): The Excel file containing APN data.
    (Form-url-encoded)

Response

    * Success (200 OK)
        * Returns a JSON object with the following data:
            + success (boolean): true if APNs are uploaded successfully, false otherwise.
            + message (string): The result message.

    * Error (Other status codes)
        + Returns an error response with the appropriate status code and error details.

## Download APNs

Download a modified version of the APN data as an Excel file.

Endpoint: /api/download

Method: GET

Response Format: Excel file download

Parameters: None

Response

    * Success (200 OK)
        + Downloads an Excel file containing the modified APN data.

    * Error (Other status codes)
        + Returns an error response with the appropriate status code and error details.

## Edit APN Data

Edit APN and Sacco data.

Endpoint: /api/edit

Method: POST

Request Format: JSON

Parameters:

    * data (array): Array of APN and Sacco objects with updated data.

Response

    * Success (200 OK)
        + Returns a JSON object with the following data:
            * success (boolean): true if the data is saved successfully, false otherwise.
            * message (string): The result message.

    * Error (Other status codes)
        + Returns an error response with the appropriate status code and error details.

## Search APNS

Search for results based on a given query.

    Endpoint: /api/search
    Method: POST
    Request Format: JSON

Parameters: 

    * search (string): The search query.
    (form-data/form-url-encoded)

Response

    * Success (200 OK)
        + Returns a JSON object with the following data:
            + success (boolean): true if results are found, false otherwise.
            + saccoResults (array): An array of Sacco results.
            + apn_numberResults (array): An array of APN number results.
            + ipResults (array): An array of IP results.

    * Error (Other status codes)
        + Returns an error response with the appropriate status code and error details.

## Delete Data

Specialized for frontend and converts the status to deleted.

    Endpoint: /api/delete
    Method: POST
    Request Format: JSON

Parameters: 

    * deletedRows (array): An array of row IDs to be deleted.

Response

    * Success (200 OK)
        + Returns a JSON object with the following data:
            + success (boolean): true if deletion is successful, false otherwise.
            + message (string): The deletion result message.

    * Error (Other status codes)
        * Returns an error response with the appropriate status code and error details.