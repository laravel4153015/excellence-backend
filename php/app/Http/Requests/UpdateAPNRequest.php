<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;


class UpdateAPNRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'apn_number' => 'sometimes|string|max:10',
            'status' => 'sometimes|in:active,inactive',
            'inactive_reason' => 'string|max:255',
            'carrier' => 'string|max:255',
            'ip' => 'string|max:255',
            'sacco_name' => 'string|max:255',
            'person_in_charge' => 'string|max:255',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param Validator $validator
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
             redirect()->back()->withErrors($validator)->withInput()
        );
    }

}
