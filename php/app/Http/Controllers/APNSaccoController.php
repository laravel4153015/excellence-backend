<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\APN;
use App\Sacco;

class APNSaccoController extends Controller
{
    public function attach(Request $request, APN $apn)
    {
        // Validate the form data
        $request->validate([
            'sacco_id' => 'required|integer|exists:saccos,id'
        ]);

        // Attach the APN to the Sacco
        $sacco = Sacco::findOrFail($request->sacco_id);
        $apn->saccos()->attach($sacco);

        return redirect()->back()->with('success', 'APN attached to Sacco successfully.');
    }

    public function detach(Request $request, APN $apn)
    {
        // Validate the form data
        $request->validate([
            'sacco_id' => 'required|integer|exists:saccos,id'
        ]);

        // Detach the APN from the Sacco
        $sacco = Sacco::findOrFail($request->sacco_id);
        $apn->saccos()->detach($sacco);

        return redirect()->back()->with('success', 'APN detached from Sacco successfully.');
    }
}
