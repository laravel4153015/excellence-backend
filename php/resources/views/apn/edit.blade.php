<!DOCTYPE html>
<html>
<head>
  <title>Edit APNs</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/handsontable@9.0.2/dist/handsontable.full.min.css"> 
</head>
<body>

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header" style="text-align: center; font-size: 40px;">Edit Data</div>
          <div class="card-body">
            <div id="data-table"></div>
            <br>
            <button id="save-button" class="btn btn-primary">Save Changes</button>
            <button id="new-record-button" class="btn btn-primary">New Record</button><br>
            <br>
          </div>
        </div>
      </div>
    </div>

    <a href="{{ url('/download') }}" class="btn btn-primary" target="_blank">Download</a>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/handsontable@9.0.2/dist/handsontable.full.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/he@1.2.0/he.js"></script>

    <script>
      $(document).ready(function() {
        let hot = null; 
        const loadData = function(){
          // Initialize Handsontable
          var data = [
              @foreach ($data as $apn)
                @foreach ($apn->saccos as $sacco)
                    {
                        'id': "{{ $apn->id }}",
                        'sacco_id': "{{ $sacco->id}}",
                        'sacco_name': he.decode("{{ $sacco->sacco_name }}"),
                        'person_in_charge': "{{ $sacco->person_in_charge }}",
                        'apn_number': "{{ $apn->apn_number }}",
                        'status': "{{ $apn->status }}",
                        'inactive_reason': "{{ $apn->inactive_reason }}",
                        'carrier': "{{ $apn->carrier }}",
                        'ip': he.decode("{{ $apn->ip }}")
                    },
                @endforeach
              @endforeach
          ];
          var container = document.getElementById('data-table');
          hot = new Handsontable(container, {
            data: data,
            rowHeaders: true,
            colHeaders: ['No.','Sacco_id','Sacco Name', 'Person In Charge', 'apn_number', 'Status', 'Inactive Reason', 'Carrier', 'IP', ''],
            colWidths: [50,50,200, 200, 150, 100, 200, 150, 150, 80],
            columns: [
              { data: 'id', readOnly: true },
              { data: 'sacco_id', readOnly: true },
              { data: 'sacco_name', readOnly: false },
              { data: 'person_in_charge', readOnly: false },
              { data: 'apn_number', readOnly: false },
              {
                data: 'status',
                readOnly: false,
                renderer: 'dropdown',
                editor: 'select',
                selectOptions: ['Active', 'Inactive']
              },
              { data: 'inactive_reason', readOnly: false },
              { data: 'carrier', readOnly: false },
              { data: 'ip', readOnly: false },
            ],
            dropdownMenu: true,            
            contextMenu: {
              callback: function(key, options) {
                if (key === 'remove_row') {
                  var selectedRows = hot.getSelected(); // Get the selected rows
                  var rowsToDelete = [];

                  // Iterate over the selected rows and collect their IDs
                  for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var rowData = hot.getDataAtRow(row); // Use getDataAtRow() method instead
                    rowsToDelete.push(rowData.id);
                  }

                  // Send an AJAX request to delete the rows in the backend
                  $.ajax({
                    type: 'POST',
                    url: '/delete-rows',
                    data: { ids: rowsToDelete },
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                      // Handle success response if needed
                      console.log('Rows deleted successfully');
                      hot.alter('remove_row', selectedRows); // Remove the selected rows from the Handsontable instance
                    },
                    error: function(xhr, status, error) {
                      // Handle error response if needed
                      console.error('Error deleting rows:', error);
                    }
                  });
                }
              },
              items: {
                'remove_row': {
                  name: 'Remove Row',
                  callback: function() {
                    // Callback function for the "Remove Row" option
                    // This code is not used directly, as we handle the deletion in the contextMenu callback above
                  }
                }
              }
            },
            licenseKey: 'non-commercial-and-evaluation',
            hiddenColumns: {
              columns: [0,1],
            },
            columnSorting:true
          });
        }
        loadData();
        

        // Handle form submission
        $('#save-button').click(function() {
          var data = hot.getData();
          for (var i = 0; i < data.length; i++) {
            var row = data[i];
            var statusCell = hot.getCell(i, 5); // Assuming 'status' column is at index 5
            row.status = statusCell.textContent;
          }
          
          // Save the data to the database
          $.ajax({
            type: 'POST',
            url: '/save-data',
            data: JSON.stringify(data),
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
              'Content-Type': 'application/json'
            },
            success: function(response) {
              location.reload();
              // loadData();
            },
            error: function(xhr, status, error) {
              var responseData = xhr.responseJSON;
              // Handle error response
              let response = 'Error saving data:' + '\n';
              for (const [key, value] of Object.entries(responseData.errors)) {
                response += "\n" + `${key}: ${value[0]}` + "\n";
              };
              alert(response + '\n' + "On row: " + responseData.row);
            }
          });
        });

        // Handle "New Record" button click
        $('#new-record-button').click(function() {
          hot.alter('insert_row');
        });
        
      });
    </script>
  </body>
</html>
