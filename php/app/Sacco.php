<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sacco extends Model
{
    protected $table = 'saccos';
    protected $fillable = ['sacco_name','person_in_charge'];
    public function apns() {
        return $this->belongsToMany(APN::class, 'a_p_n_sacco', 'sacco_id', 'apn_id')->withTimestamps();
    }    
}
