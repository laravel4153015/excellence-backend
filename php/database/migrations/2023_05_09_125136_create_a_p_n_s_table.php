<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAPNSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('a_p_n_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('apn_number');
            $table->string('ip')->nullable();
            $table->string('date_deployed')->nullable();
            $table->string('status')->default('Active');
            $table->string('inactive_reason')->nullable();
            $table->string('carrier')->nullable();
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('a_p_n_s');
    }
}
