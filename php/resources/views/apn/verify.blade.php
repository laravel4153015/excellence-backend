<!DOCTYPE html>
<html>
    <head>
        <title>APN Verification</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
        <h1>APN Verification</h1>
        <form method="POST" action="{{ route('verify') }}" enctype="multipart/form-data">
            @csrf
            <div>
                <label for="file">Select File:</label>
                <input type="file" name="file" id="file"><br>
            </div>
            <button type="submit">Verify APNs</button>
        </form>
        @if (session('message'))
            <p>{{ session('message') }}</p>
        @endif
        @if (session('errors'))
            <p>APNs do not exist in the database:</p>
            <ul>
                @foreach (session('errors') as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>
