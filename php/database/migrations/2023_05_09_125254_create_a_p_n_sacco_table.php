<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAPNSaccoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('a_p_n_sacco', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sacco_id');
            $table->unsignedBigInteger('apn_id');
            $table->timestamps();

            $table->foreign('sacco_id')->references('id')->on('saccos');
            $table->foreign('apn_id')->references('id')->on('a_p_n_s');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('a_p_n_sacco');
    }
}
