<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sacco;

class SaccoController extends Controller
{
    public function index()
    {
        // Retrieve all saccos
        $saccos = Sacco::all();

        return view('saccos.index', compact('saccos'));
    }

    public function create()
    {
        // Show the form to create a new sacco
        return view('saccos.create');
    }

    public function store(Request $request)
    {
        // Validate the form data
        $request->validate([
            'name' => 'required|string|unique:saccos'
        ]);

        // Create a new Sacco instance and save it to the database
        $sacco = new Sacco;
        $sacco->name = $request->name;
        $sacco->save();

        return redirect()->route('saccos.index')->with('success', 'Sacco created successfully.');
    }

    public function edit(Sacco $sacco)
    {
        // Show the form to edit an existing sacco
        return view('saccos.edit', compact('sacco'));
    }

    public function update(Request $request, Sacco $sacco)
    {
        // Validate the form data
        $request->validate([
            'name' => 'required|string|unique:saccos,name,'.$sacco->id
        ]);

        // Update the Sacco instance and save it to the database
        $sacco->name = $request->name;
        $sacco->save();

        return redirect()->route('saccos.index')->with('success', 'Sacco updated successfully.');
    }

    public function destroy(Sacco $sacco)
    {
        // Delete the Sacco instance
        $sacco->delete();

        return redirect()->route('saccos.index')->with('success', 'Sacco deleted successfully.');
    }
}
